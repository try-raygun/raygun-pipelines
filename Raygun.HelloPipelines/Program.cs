﻿using System;
using Mindscape.Raygun4Net;

namespace Raygun.HelloPipelines
{
  public class Program
  {
    public static void Main(string[] args)
    {
      try
      {
        Console.WriteLine("Hello Pipelines");
      }
      catch (Exception e)
      {
        new RaygunClient(new RaygunSettings
        {
          ApiKey = "[your-api-key-here]",
          ApplicationVersion = "1.0.1"
        }).Send(e);
      }
    }
  }
}
