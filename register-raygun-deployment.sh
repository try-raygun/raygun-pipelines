﻿#!/bin/bash -ex

# Generate an External Access Token for your build server to use from your Raygun user profile page https://app.raygun.com/user
# Set the following environment variables.

# RAYGUN_AUTH_TOKEN      (Required) External Access Token for the Raygun user.
# RAYGUN_API_KEY         (Required) The API Key for your Raygun Application.
# DEPLOYMENT_VERSION     (Required) version string for this deployment.
# DEPLOYED_BY            (Optional) the name of the person who created the deployment.
# EMAIL_ADDRESS          (Optional) the email address of the person who created the deployment.
# BITBUCKET_COMMIT       (Provided) Commit SHA that triggered the build.
# DEPLOYMENT_NOTES       (Optional) the release notes for this deployment. Will be formatted using a Markdown parser.

if [ -z "${DEPLOYED_BY}" ]; then
    DEPLOYED_BY=${BITBUCKET_REPO_OWNER}
fi

url="https://app.raygun.com/deployments?authToken=$RAYGUN_AUTH_TOKEN"

read -d '' deployment <<- EOF
{
    apiKey: \"$RAYGUN_API_KEY\",
    version: \"$DEPLOYMENT_VERSION\",
    ownerName: \"$DEPLOYED_BY\",
    emailAddress: \"$EMAIL_ADDRESS\",
    scmIdentifier: \"$BITBUCKET_COMMIT\",
    scmType: \"BitBucket\",
    comment: \"$DEPLOYMENT_NOTES\" 
}
EOF

curl -H "Content-Type: application/json" -d "$deployment" $url

if [ "$?" -ne "0" ]; then
  echo "Could not send deployment details to Raygun"
  exit 1
fi