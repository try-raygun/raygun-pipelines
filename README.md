# Register Deployment in Raygun #

Deployment tracking will essentially allow you to timestamp your deployments, notifying Raygun when you release your software. Discover problematic deployments and see the commits that made up each of them. You'll then be able to easily see which errors occurred after your latest deployment, which were fixed, and which are still occurring.

This repository contains a sample .NET Core application and an example deployment script that is executed by Bitbucket Pipelines which registers a new deployment in Raygun.

Read all about [Raygun deployment tracking](https://raygun.com/blog/2015/03/deployment-tracking-arrives-in-raygun/)

## How to configure Pipelines ##

* Add the required Environment Variables below to your Pipelines settings of your Bitbucket repository.
* Add `register-raygun-deployment.sh` to your repository ([View register-raygun-deployment.sh](https://bitbucket.org/try-raygun/raygun-pipelines/src/master/register-raygun-deployment.sh?at=master&fileviewer=file-view-default)).
* Copy `bitbucket-pipelines.yml` to your project.
	+ Or use your own, just be sure to include all steps in the sample yml file.

## Environment variables ##

* `RAYGUN_AUTH_TOKEN`: (Required) External Access Token for the Raygun user.
* `RAYGUN_API_KEY`: (Required) The API Key for your Raygun Application.
* `DEPLOYMENT_VERSION`: (Required) version string for this deployment.
* `DEPLOYED_BY`: (Optional) the name of the person who created the deployment.
* `EMAIL_ADDRESS`: (Optional) the email address of the person who created the deployment.
* `BITBUCKET_COMMIT`: (Provided) Commit SHA that triggered the build.
* `DEPLOYMENT_NOTES`: (Optional) the release notes for this deployment. Will be formatted using a Markdown parser.